package space.ayin.executor;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import space.ayin.lib.AyinConnection;
import space.ayin.lib.AyinDialogueImpl;
import space.ayin.lib.AyinJmsConnection;
import space.ayin.lib.AyinToken;
import space.ayin.runtime.runtimemodel.AyinCallRequest;
import space.ayin.runtime.runtimemodel.AyinCallResponse;
import space.ayin.runtime.runtimemodel.AyinExecutorInstance;
import space.ayin.runtime.runtimemodel.AyinResponse;
import space.ayin.runtime.runtimemodel.AyinRuntimeModelFactory;
import space.ayin.runtime.runtimemodel.AyinRuntimeModelPackage;
import space.ayin.runtime.runtimemodel.ExecutorConnectionDescriptor;
import space.ayin.runtime.runtimemodel.ExecutorProfile;
import space.ayin.runtime.runtimemodel.GetExecutorInvoke;
import space.ayin.runtime.runtimemodel.GetExecutorInvokeResult;
import space.ayin.runtime.runtimemodel.JmsConnectionDescriptor;
import space.ayin.runtime.utils.AyinRuntimeUtils;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.Future;

public class ExecutorApplication {

    private static Logger logger = LoggerFactory.getLogger(ExecutorApplication.class);

    private Options options;

    private static ExecutorApplication instance;

    private ExecutorProfile executorProfile;

    public static void main(String[] args) {
        instance = new ExecutorApplication();
        instance.run(args);
    }

    public void run(String[] args) {
        options = new Options();
        options.addOption("w", "workspace", true, "workspace location");

        CommandLineParser parser = new DefaultParser();
        CommandLine line;
        try {
            line = parser.parse(options, args);

            String workspaceLocation = null;
            if (line.hasOption("w")) {
                workspaceLocation = line.getOptionValue("w");
            } else {
                workspaceLocation = System.getProperty("user.home") + File.separator + "ayinexecutor";
            }

            initializeWorkspace(workspaceLocation);

        } catch (ParseException e) {
            logger.error("Parsing args failed : {}", e.getMessage());
        }
    }

    public void run(ExecutorProfile executorProfile) {
        this.executorProfile = executorProfile;
    }

    public void run(String workspaceLocation, boolean gui) {
        initializeWorkspace(workspaceLocation);
    }

    private void initializeWorkspace(String workspaceLocation) {
        AyinRuntimeModelPackage.eINSTANCE.eClass();

        ResourceSet executorProfileResourceSet;

        executorProfileResourceSet = new ResourceSetImpl();
        executorProfileResourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xml", new XMLResourceFactoryImpl());

//        Path profilePath = Paths.get(workspaceLocation, "profile.xml");
        File profile = new File(workspaceLocation, "profile.xml");

        if (!profile.exists()) {
            executorProfile = AyinRuntimeModelFactory.eINSTANCE.createExecutorProfile();
            Resource resource = executorProfileResourceSet.createResource(
                    URI.createFileURI(profile.getAbsolutePath())
            );
            resource.getContents().add(executorProfile);
            try {
                resource.save(Collections.emptyMap());
            } catch (IOException e) {
                throw new RuntimeException("can not create executorProfile in workspace", e);
            }
        } else {
            Resource resource = executorProfileResourceSet.createResource(
                    URI.createFileURI(profile.getAbsolutePath())
            );
            try {
                resource.load(Collections.emptyMap());
            } catch (IOException e) {
                throw new RuntimeException("can not read executorProfile in workspace", e);
            }
            executorProfile = (ExecutorProfile) resource.getContents().get(0);
        }


    }


    public static ExecutorApplication getInstance() {
        return instance;
    }

    public ExecutorProfile getProfile() {
        return executorProfile;
    }


    private static void authorizeConnection(AyinConnection connection) {
        try {
            GetExecutorInvoke getExecutorInvoke = AyinRuntimeModelFactory.eINSTANCE.createGetExecutorInvoke();
            getExecutorInvoke.setKey(connection.getMyToken().getToken());
            AyinCallRequest ayinCallRequest = AyinRuntimeUtils.packToCallMessage(getExecutorInvoke);

            AyinToken recipientToken = new AyinToken("");
            AyinDialogueImpl dialogue = connection.getDialogue(recipientToken);

            Future<? extends AyinResponse> call = dialogue.call(ayinCallRequest, connection.getAnswerTimeout());

            AyinCallResponse response = (AyinCallResponse) call.get();
            GetExecutorInvokeResult getExecutorInvokeResult = (GetExecutorInvokeResult) response.getInvokeReturn();

            logger.info("authorized as {}", getExecutorInvokeResult.getExecutorName());

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public AyinConnection createAyinConnection(AyinExecutorInstance executor, String descriptorName) throws Exception {


        ExecutorConnectionDescriptor connectionDescriptor = null;
        for (ExecutorConnectionDescriptor executorConnectionDescriptor : getProfile().getConnectionDescriptors()) {
            if (executorConnectionDescriptor.getName().equals(descriptorName)) {
                connectionDescriptor = executorConnectionDescriptor;
                break;
            }
        }
        if (connectionDescriptor == null) {
            throw new RuntimeException("descriptor " + descriptorName + " not found in profile");
        }


        if (connectionDescriptor instanceof JmsConnectionDescriptor) {
            JmsConnectionDescriptor jmsConnectionDescriptor = (JmsConnectionDescriptor) connectionDescriptor;

            Connection connection = this.createConnection(jmsConnectionDescriptor);
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);


//            Destination inputQueue = session.createTemporaryQueue();
            Destination inputQueue = session.createQueue(((JmsConnectionDescriptor) connectionDescriptor).getKey());

            Destination outputQueue = session.createQueue(jmsConnectionDescriptor.getOutputQueue());

            AyinConnection ayinConnection = new AyinJmsConnection(
                    executor,
                    new AyinToken(jmsConnectionDescriptor.getKey()),
                    session,
                    outputQueue,
                    inputQueue,
                    connectionDescriptor.getAnswerTimeout()
            );
            authorizeConnection(ayinConnection);

            return ayinConnection;
        } else {
            throw new RuntimeException("unsupported connection type " + connectionDescriptor);
        }
    }

    protected Connection createConnection(JmsConnectionDescriptor jmsConnectionDescriptor) throws JMSException {
        return new ActiveMQConnectionFactory(jmsConnectionDescriptor.getLogin(), jmsConnectionDescriptor.getPassword(), jmsConnectionDescriptor.getUrl())
                .createConnection();
    }

}
