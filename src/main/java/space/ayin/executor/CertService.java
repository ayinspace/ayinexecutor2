package space.ayin.executor;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class CertService {

    private KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");

    private SecureRandom random = new SecureRandom();

    public CertService() throws NoSuchAlgorithmException {
        keyGen.initialize(2048, random);
    }

    public KeyPair generateKeyPair() {
        return keyGen.generateKeyPair();
    }
}
