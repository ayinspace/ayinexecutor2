package space.ayin.lib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import space.ayin.runtime.runtimemodel.AyinExecutorInstance;
import space.ayin.runtime.runtimemodel.AyinMessage;
import space.ayin.runtime.utils.AyinRuntimeUtils;

import javax.jms.*;

public class AyinJmsConnection extends AyinConnection {

    private static Logger logger = LoggerFactory.getLogger(AyinJmsConnection.class);

    private final Session session;

    private final Destination inputDestination;

    private final Destination outputDestination;

    private final MessageConsumer messageConsumer;


    public AyinJmsConnection(final AyinExecutorInstance ayinExecutorInstance, final AyinToken myToken, final Session session, final Destination outputDestination, final Destination inputDestination, long answerTimeout) {
        super(ayinExecutorInstance, myToken, answerTimeout);
        this.session = session;
        this.inputDestination = inputDestination;
        this.outputDestination = outputDestination;


        this.outputCourier = new AyinJmsCourier(session, outputDestination, inputDestination);
        try {
            this.messageConsumer = session.createConsumer(inputDestination);
            messageConsumer.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message receivedMessage) {
                    try {
                        processMessage(receivedMessage);
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                }
            });
        } catch (JMSException e) {
            throw new RuntimeException(e.getMessage(), e);
        }

    }

    private static AyinMessage deserializeMessage(Message message) throws JMSException {
        if (message instanceof TextMessage) {
            String text = ((TextMessage) message).getText();
            return (AyinMessage) AyinRuntimeUtils.deserializeFromXml(text);
        } else if (message instanceof BytesMessage) {
            byte[] data = new byte[(int)(((BytesMessage) message).getBodyLength())];
            ((BytesMessage) message).readBytes(data);

            try {
                return (AyinMessage) AyinRuntimeUtils.deserializeFromXml(new String(data));
            } catch (RuntimeException e) {
                return (AyinMessage) AyinRuntimeUtils.deserializeFromBinary(data);
            }
        } else {
            throw new RuntimeException("unsupported message type " + message);
        }
    }

    protected void processMessage(Message receivedMessage) throws JMSException {
        AyinMessage receivedAyinMessage = deserializeMessage(receivedMessage);
        AyinToken senderToken = new AyinToken(receivedAyinMessage.getToken());

        AyinToken recipientToken = new AyinToken(receivedAyinMessage.getRecipientToken());
        if (myToken.equals(recipientToken) || recipientToken.getToken().isEmpty()) {
            AyinDialogueImpl senderDialogue = recipientDialogRegistry.get(senderToken);
            if (senderDialogue == null) {
                if (receivedMessage.getJMSReplyTo() == null) {
                    throw new RuntimeException("replyTo is null");
                } else {
                    logger.debug("creating dialogue");
                    senderDialogue = new AyinDialogueImpl(this, senderToken, new AyinJmsCourier(session, receivedMessage.getJMSReplyTo(), inputDestination));
                }
            } else {
                if (!((AyinJmsCourier) senderDialogue.getOutputCourier()).getOutputDestination().equals(receivedMessage.getJMSReplyTo())) {//TODO: make this check abstract
                    logger.debug("re-creating dialogue");
                    senderDialogue = new AyinDialogueImpl(this, senderToken, new AyinJmsCourier(session, receivedMessage.getJMSReplyTo(), inputDestination));
                }
            }
            senderDialogue.messageReceived(receivedAyinMessage);
        } else {
            AyinDialogueImpl dialogue = recipientDialogRegistry.get(recipientToken);
            if (dialogue == null || dialogue.getOutputCourier() == null) {
                if (receivedMessage instanceof TextMessage) {
                    logger.error("can't find dialogue for {}", ((TextMessage) receivedMessage).getText());
                } else {
                    logger.error("can't find dialogue for {}", receivedMessage);
                }
                throw new RuntimeException("can't find dialogue with output courier, maybe you forgot to connect remote client first?");//TODO: send response with system exception
            }

            dialogue.getOutputCourier().send(receivedAyinMessage);//TODO: send runtime exception
        }

    }
}
