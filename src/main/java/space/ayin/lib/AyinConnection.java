package space.ayin.lib;

import space.ayin.runtime.runtimemodel.AyinExecutorInstance;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

public abstract class AyinConnection {

    protected AyinCourier outputCourier;

    protected final Map<AyinToken, AyinDialogueImpl> recipientDialogRegistry = Collections.synchronizedMap(new WeakHashMap<>());

    private final AyinExecutorInstance ayinExecutorInstance;

    protected AyinToken myToken;

    private final AyinToken emptyToken = new AyinToken("");

    protected final long answerTimeout;

    AyinConnection(final AyinExecutorInstance ayinExecutorInstance, final AyinToken myToken, final long answerTimeout) {
        this.ayinExecutorInstance = ayinExecutorInstance;
        this.myToken = myToken;
        this.answerTimeout = answerTimeout;
    }

    void addDialogue(final AyinToken recipientToken, final AyinDialogueImpl ayinDialogue) {
        recipientDialogRegistry.put(recipientToken, ayinDialogue);
    }

    public AyinDialogueImpl getDialogue(AyinToken recipientToken) {
        if (recipientDialogRegistry.containsKey(recipientToken)) {
            return recipientDialogRegistry.get(recipientToken);
        }
        return new AyinDialogueImpl(this, recipientToken, outputCourier);
    }

    public AyinToken getMyToken() {
        return myToken;
    }

    public AyinToken getEmptyToken() {
        return emptyToken;
    }

    AyinExecutorInstance getAyinExecutorInstance() {
        return ayinExecutorInstance;
    }

    public long getAnswerTimeout() {
        return answerTimeout;
    }
}
