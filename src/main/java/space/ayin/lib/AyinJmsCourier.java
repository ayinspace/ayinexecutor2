package space.ayin.lib;

import space.ayin.runtime.runtimemodel.AyinMessage;
import space.ayin.runtime.utils.AyinRuntimeUtils;

import javax.jms.*;

public class AyinJmsCourier extends AyinCourier {

    private final Session session;

    private final MessageProducer producer;

    private final Destination inputDestination;

    private final Destination outputDestination;

    public AyinJmsCourier(final Session session, final Destination outputDestination, final Destination inputDestination) {
        this.session = session;
        this.inputDestination = inputDestination;
        this.outputDestination = outputDestination;
        try {
            this.producer = session.createProducer(outputDestination);
        } catch (JMSException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void send(AyinMessage ayinMessage) {
        try {
            Message jmsMessage = createJmsMessage(ayinMessage);
            producer.send(jmsMessage);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private Message createJmsMessage(AyinMessage ayinMessage) {
        try {
            TextMessage jmsMessage = session.createTextMessage();
            jmsMessage.setJMSReplyTo(inputDestination);
            jmsMessage.setText(AyinRuntimeUtils.serializeToXml(ayinMessage));
            return jmsMessage;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public Destination getOutputDestination() {
        return outputDestination;
    }
}
