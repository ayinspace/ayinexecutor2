package space.ayin.lib;

import org.slf4j.Logger;
import space.ayin.runtime.runtimemodel.AyinInvokeResult;
import space.ayin.runtime.runtimemodel.AyinRuntimeException;
import space.ayin.runtime.runtimemodel.AyinRuntimeModelFactory;
import space.ayin.runtime.runtimemodel.impl.AyinExceptionImpl;
import space.ayin.runtime.runtimemodel.impl.AyinRuntimeExceptionImpl;

import java.io.PrintWriter;
import java.io.StringWriter;

public class AyinExecutorUtils {

    public static String stackTraceToString(Exception e) {
        StringWriter writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        e.printStackTrace(printWriter);
        printWriter.flush();
        return printWriter.toString();
    }

    public static AyinRuntimeException createAyinRuntimeException(Exception exception) {
        AyinRuntimeExceptionImpl ayinRuntimeException = (AyinRuntimeExceptionImpl) AyinRuntimeModelFactory.eINSTANCE.createAyinRuntimeException();
        ayinRuntimeException.setLocalizedMessage(exception.getMessage());
        ayinRuntimeException.setDetailedMessage(AyinExecutorUtils.stackTraceToString(exception));
        return ayinRuntimeException;
    }

    public static void processExeption(Exception e, Logger logger, AyinInvokeResult result) {
        if (e instanceof AyinExceptionImpl) {
            logger.debug(e.getMessage(), e);
            result.set_throw((AyinExceptionImpl) e);
        } else {
            logger.warn(e.getMessage(), e);
            result.set_throw(space.ayin.lib.AyinExecutorUtils.createAyinRuntimeException(e));
        }
    }

}
