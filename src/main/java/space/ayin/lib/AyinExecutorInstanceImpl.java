package space.ayin.lib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import space.ayin.runtime.runtimemodel.*;
import space.ayin.runtime.runtimemodel.impl.AyinRuntimeExceptionImpl;

import java.io.PrintWriter;
import java.io.StringWriter;

public class AyinExecutorInstanceImpl extends space.ayin.runtime.runtimemodel.impl.AyinExecutorInstanceImpl {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public AyinInvokeResult invoke(AyinInvoke ayinInvoke) {
        if (ayinInvoke instanceof AyinSystemInvoke) {
            return systemInvoke((AyinSystemInvoke) ayinInvoke);
        }
        throw new RuntimeException("unhandled invoke " + ayinInvoke);
    }

    @Override
    public AyinSystemInvokeResult systemInvoke(AyinSystemInvoke ayinSystemInvoke) {
        if (ayinSystemInvoke instanceof GetExecutorInvoke) {
            GetExecutorInvokeResult getExecutorInvokeResult = AyinRuntimeModelFactory.eINSTANCE.createGetExecutorInvokeResult();
            getExecutorInvokeResult.setExecutorName("ExecutorBlablabla");//TODO: return reflection model of executor from executors repository
            getExecutorInvokeResult.setToken("TokenBlablabla");
            return getExecutorInvokeResult;
        } else if (ayinSystemInvoke instanceof SessionInvoke) {
            //FIXME
        }
        throw new RuntimeException("system method not implemented yet " + ayinSystemInvoke);
    }

    @Override
    public Logger getLogger() {
        return logger;
    }
}
