package space.ayin.lib;

import java.util.Objects;

public class AyinToken {

    private final String token;

    public AyinToken(final String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AyinToken ayinToken = (AyinToken) o;
        return Objects.equals(token, ayinToken.token);
    }

    @Override
    public int hashCode() {

        return Objects.hash(token);
    }
}
