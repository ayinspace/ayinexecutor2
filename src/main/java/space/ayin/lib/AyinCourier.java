package space.ayin.lib;

import space.ayin.runtime.runtimemodel.AyinMessage;

public abstract class AyinCourier {
    public abstract void send(AyinMessage ayinMessage);
}
