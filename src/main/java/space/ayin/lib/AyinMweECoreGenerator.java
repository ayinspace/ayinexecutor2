package space.ayin.lib;

import com.google.inject.Injector;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.mwe2.ecore.EcoreGenerator;
import space.ayin.lang.AyinStandaloneSetup;

public class AyinMweECoreGenerator extends EcoreGenerator {

    private Injector injector = new AyinStandaloneSetup().createInjector();

    public AyinMweECoreGenerator() {
        super();
        setResourceSet(injector.getInstance(ResourceSet.class));
    }
}
