package space.ayin.lib;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import space.ayin.runtime.runtimemodel.*;
import space.ayin.runtime.utils.AyinRuntimeUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;

public class AyinDialogueImpl extends MinimalEObjectImpl.Container implements AyinDialogue {

    private static Logger logger = LoggerFactory.getLogger(AyinDialogueImpl.class);

    private static ExecutorService threadPool = Executors.newCachedThreadPool();

    private final AyinCourier outputCourier;

    private final AyinToken recipientToken;

    private final AyinConnection connection;

    private AtomicLong sequenceGen = new AtomicLong();

    private volatile Map<Long, RequestMonitor> requests = Collections.synchronizedMap(new HashMap<>());

    private volatile Map<Long, RequestMonitor> tasks = Collections.synchronizedMap(new HashMap<>());


    AyinDialogueImpl(final AyinConnection connection, final AyinToken recipientToken, final AyinCourier outputCourier) {
        this.connection = connection;
        this.recipientToken = recipientToken;
        this.outputCourier = outputCourier;
        connection.addDialogue(recipientToken, this);
    }

    @Override
    public Future<? extends AyinResponse> call(AyinRequest request, long answerTimeout) {
        long sequence = sequenceGen.incrementAndGet();

        request.setSequence(sequence);
        request.setToken(connection.getMyToken().getToken());
        request.setRecipientToken(recipientToken.getToken());

        RequestMonitor requestMonitor = new RequestMonitor();
        requestMonitor.request = request;

        requests.put(sequence, requestMonitor);

        requestMonitor.responseFuture = threadPool.submit(() -> {
            try {
                while (true) {
                    synchronized (requestMonitor) {
                        if (requestMonitor.response != null) {
                            requests.remove(sequence);
                            return requestMonitor.response;
                        }
                        requestMonitor.wait(answerTimeout);
                    }
                    if (requestMonitor.response != null) {
                        requests.remove(sequence);
                        return requestMonitor.response;
                    } else {
                        if (request instanceof AyinAckRequest) {
                            throw new RuntimeException("ack " + sequence + " not answered");
                        } else if (request instanceof AyinCallRequest) {
                            logger.trace("ack {}...", sequence);
                            AyinAckRequest ackRequest = AyinRuntimeModelFactory.eINSTANCE.createAyinAckRequest();
                            ackRequest.setTargetSequence(sequence);
                            AyinAckResponse ackResponse = (AyinAckResponse) call(ackRequest, answerTimeout).get();
                            logger.trace("ack response: {}, {}", sequence, ackResponse);
                            if (!ackResponse.isRequestProcessing()) {
                                throw new RuntimeException("remote " + sequence + " is dead");
                            }
                        } else {
                            throw new RuntimeException("call logic is not implemented for " + request);
                        }
                    }
                }
            } catch (Exception e) {
                logger.warn(e.getMessage(), e);
                requests.remove(sequence);
                throw new RuntimeException(e.getMessage(), e);
            }

        });

        outputCourier.send(request);

        return requestMonitor.responseFuture;
    }

    @Override
    public Future call(AyinRequest ayinRequest) {
        return call(ayinRequest, getConnection().getAnswerTimeout());
    }

    void messageReceived(AyinMessage receivedMessage) {
        if (receivedMessage instanceof AyinRequest) {
            threadPool.submit(() -> {
                try {
                    AyinResponse response;
                    if (receivedMessage instanceof AyinCallRequest) {
                        AyinInvokeResult ayinInvokeResult = callRequestReceived((AyinCallRequest) receivedMessage);
                        response = AyinRuntimeUtils.packToCallResponseMessage(ayinInvokeResult);
                    } else if (receivedMessage instanceof AyinAckRequest) {
                        long targetSequence = ((AyinAckRequest) receivedMessage).getTargetSequence();
                        response = AyinRuntimeModelFactory.eINSTANCE.createAyinAckResponse();
                        ((AyinAckResponse) response).setRequestProcessing((tasks.containsKey(targetSequence)));
                    } else {
                        throw new RuntimeException("unhandled message " + receivedMessage);
                    }

                    response.setSequence(receivedMessage.getSequence());
                    if (!receivedMessage.getRecipientToken().isEmpty()) {
                        response.setToken(connection.getMyToken().getToken());
                    } else {
                        response.setToken(connection.getEmptyToken().getToken());
                    }
                    response.setRecipientToken(receivedMessage.getToken());

                    outputCourier.send(response);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            });

        } else {
            responseReceived((AyinResponse) receivedMessage);
        }
    }

    private AyinInvokeResult callRequestReceived(AyinCallRequest callRequest) {
        logger.debug("call request received: {}", callRequest);
        try {
            RequestMonitor taskMonitor = new RequestMonitor();
            taskMonitor.request = callRequest;
            tasks.put(callRequest.getSequence(), taskMonitor);
            logger.trace("invoke: {}", callRequest.getInvoke());
            AyinInvokeResult invoke = connection.getAyinExecutorInstance().invoke((callRequest).getInvoke());
            if (invoke.getException() != null) {
                throw invoke.getException();
            }
            return invoke;
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        } finally {
            tasks.remove(callRequest.getSequence());
        }
    }

    private void responseReceived(AyinResponse response) {
        RequestMonitor requestResponse = requests.get(response.getSequence());
        if (requestResponse == null) {
            logger.error("request with seq {} not found in requests registry: {}", response.getSequence(), response);
        } else {
            synchronized (requestResponse) {
                requestResponse.response = response;
                requestResponse.notify();
            }
        }
    }

    public AyinCourier getOutputCourier() {
        return outputCourier;
    }

    private class RequestMonitor {
        private AyinRequest request;
        private volatile AyinResponse response;
        private Future<AyinResponse> responseFuture;
    }

    public AyinConnection getConnection() {
        return connection;
    }
}
